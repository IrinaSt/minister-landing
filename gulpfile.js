
const gulp = require('gulp'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    clean = require('gulp-clean'),
    browserSync = require('browser-sync'),
    minifyjs = require('gulp-js-minify'),
    cleanCSS = require('gulp-clean-css');

const path = {
    build: {
        html: 'public/',
        css: 'public/css/',
        js: 'public/js/',
        img: 'public/img/',
        fonts: 'public/fonts/'

    },
    src: {
        html: 'src/index.html',
        scss: 'src/scss/**/style.scss',
        js: 'src/js/*.js',
        img: 'src/img/**/*',
        fonts: 'src/fonts/**/*'
    },
    clean: './public/'
};

const scssBuild = () => {
    return gulp.src(path.src.scss)
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            overrideBrowserslist: ['last 100 versions'],
            cascade: false
        }))
        .pipe(gulp.dest(path.build.css))

};

const jsBuild = () => {
    return gulp.src(path.src.js)
        .pipe(gulp.dest(path.build.js))
};

const htmlBuild = () => {
    return gulp.src(path.src.html)
        .pipe(gulp.dest(path.build.html))
};

const cleanBuild = () => {
    return gulp.src(path.clean, { allowEmpty: true })
        .pipe(clean())
};
const jsMinify = () => {
    return gulp.src('build/js/script.js', { allowEmpty: true })
        .pipe(minifyjs())
        .pipe(gulp.dest(path.build.js));
};
const fontsBuild = async function () {
    gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts))
};

const cssMinify = () => {
    return gulp.src(path.build.css)
        .pipe(cleanCSS({ compatibility: 'ie8' }))
        .pipe(gulp.dest(path.build.css));
};
const imgMinify = () => {
    return gulp.src(path.src.img)
        .pipe(gulp.dest(path.build.img))
};
const watcher = () => {
    browserSync.init({
        server: {
            baseDir: './build'
        }
    });
    gulp.watch(path.src.html, htmlBuild).on('change', browserSync.reload);
    gulp.watch(path.src.scss, scssBuild).on('change', browserSync.reload);
    gulp.watch(path.src.js, jsBuild).on('change', browserSync.reload);
    gulp.watch(path.src.js, jsMinify).on('change', browserSync.reload);
    gulp.watch(path.src.img, imgMinify).on('change', browserSync.reload);
};
gulp.task('build', gulp.series(
    cleanBuild,
    fontsBuild,
    htmlBuild,
    scssBuild,
    cssMinify,
    jsBuild,
    jsMinify,
    imgMinify
));

gulp.task('watch', gulp.series(
    cleanBuild,
    fontsBuild,
    htmlBuild,
    scssBuild,
    cssMinify,
    jsBuild,
    jsMinify,
    imgMinify,
    watcher,
));