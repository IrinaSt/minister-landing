new Glider(document.querySelector('.glider'), {
  slidesToShow: 1,
  slidesToScroll: 1,
  draggable: true,
  arrows: {
    prev: '.glider-prev',
    next: '.glider-next'
  },
  responsive: [
 {
      // screens greater than >= 1024px
      breakpoint: 1024,
      settings: {
        slidesToShow: 4,
        slidesToScroll: 1,
        itemWidth: 150,
        duration: 0.25
      }
    }
  ]
});

document.querySelectorAll('.header-menu a').forEach(element => {
  element.addEventListener('click', function(event){
    event.preventDefault();
    const element = this.getAttribute('href');
    const sectionPosition = document.getElementById(element).offsetTop;
    scrollToElement(sectionPosition);
  })
});

document.getElementById('scrollToTopBtn').addEventListener('click', function(){
  scrollToElement(0);
});

function scrollToElement(elementPosition) {
  window.scrollTo({
    top: elementPosition,
     behavior: "smooth"
 });
};

document.getElementById('burgerMenu').addEventListener('click', function (){
  document.getElementById('headerMenu').classList.toggle('show');
})
